" Vim syntax file
" Language:   Spade

if exists("b:current_syntax") && b:current_syntax == "spade"
    finish
endif
let b:current_syntax = "spade"

syn keyword spadeKeyword entity fn reg let reset inst enum decl struct use as mod stage pipeline assert
syn keyword spadeConditional match if else
syn keyword spadeBool true false
syn keyword spadeType bool int bitvector bit
syn keyword spadeEnum Option
syn keyword spadeEnumVariant Some None

syn match spadeIdent       "[A-Za-z0-9_][A-Za-z0-9_]\+"
syn match spadeModPath     "[A-Za-z0-9_]\([A-Za-z0-9_]\)*::[^<]"he=e-3,me=e-3
syn match spadeModPathSep  "::"

syn match spadeDecimal '[0-9][0-9_]*'
syn match spadeNumber '0x[a-f0-9_]*'
syn match spadeNumber '0b[0-1_]*'
hi def link spadeDecimal spadeNumber
hi def link spadeHexadecimal spadeNumber
hi def link spadeBinary spadeNumber

syn keyword spadeTodo contained TODO FIXME XXX NOTE
syn match spadeComment "//.*$" contains=spadeTodo

syn match     spadeOperator     display "\%(+\|-\|*\|=\|&\||\|!\|>\|<\|%\)=\?"

syn match spadeLabel "'[A-Za-z0-9_]\+"

hi def link spadeEnum        spadeType
hi def link spadeEnumVariant spadeConstant

hi def link spadeTodo        Todo
hi def link spadeComment     Comment
hi def link spadeKeyword     Keyword
" The themes I've tried highlight identifiers separately, which looks very odd
" when most things are identifiers, because of that, the Identifier link is
" commented out
" hi def link spadeIdent       Identifier
hi def link spadeModPath     Include
hi def link spadeConditional Conditional

hi def link spadeBool        Boolean
hi def link spadeNumber      Constant
hi def link spadeConstant    Constant

hi def link spadeOperator    Operator
hi def link spadeType        Type

hi def link spadeLabel       Label

