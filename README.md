# spade-vim

Vim syntax highlighting and other things for the spade language

The plugin works out of the box, but the legacy vim highlights are not actively maintained. Using tree-sitter is recommended

Install https://github.com/nvim-treesitter/nvim-treesitter in addition to this plugin, then configure it to use the Spade tree-sitter grammar using

```lua
local parser_config = require'nvim-treesitter.parsers'.get_parser_configs()
require'nvim-treesitter.install'.prefer_git = true

parser_config.spade = {
  install_info = {
    url = "https://gitlab.com/spade-lang/tree-sitter-spade/", -- local path or git repo
    files = {"src/parser.c"},
    -- optional entries:
    branch = "main", -- default branch in case of git repo if different from master
    generate_requires_npm = false, -- if stand-alone parser without npm dependencies
    requires_generate_from_grammar = false, -- if folder contains pre-generated src/parser.c
  },
  filetype = "spade", -- if filetype does not match the parser name
}

require'nvim-treesitter.configs'.setup {}
```

Finally, install the Spade tree-sitter parser using `:TSInstall spade`
